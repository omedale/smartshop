
(function(){
  'use strict';
  var module = angular.module('app', ['onsen']);
 

       


 
})();

// document.addEventListener("deviceready",onDeviceReady,false);

 function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }

     function onPhotoDataSuccess(imageData) {
      // Uncomment to view the base64-encoded image data
      // console.log(imageData);

      // Get image handle
      //
      var smallImage = document.getElementById('smallImage');

      // Unhide image elements
      //
      smallImage.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      //
      smallImage.src = "data:image/jpeg;base64," + imageData;
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
      // Uncomment to view the image file URI
      // console.log(imageURI);

      // Get image handle
      //
      var largeImage = document.getElementById('largeImage');

      // Unhide image elements
      //
      largeImage.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      //
      largeImage.src = imageURI;
    }

    // A button will call this function
    //
    function capturePhoto() {
      // Take picture using device camera and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    //
    function capturePhotoEdit() {
      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    //
    function getPhoto(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
    }

    // Called if something bad happens.
    //
    function onFail(message) {
      alert('Failed because: ' + message);
    }


var fasttrackdb = {};
        fasttrackdb.webdb = {};
        fasttrackdb.webdb.db = null;

        fasttrackdb.webdb.open = function () {
            var dbSize = 5 * 1024 * 1024; // 5MB
            fasttrackdb.webdb.db = openDatabase("FastTrack", "1.0", "FastTrack", dbSize);
        }

        fasttrackdb.webdb.createTable = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY ASC, phone TEXT, fullname TEXT,address TEXT, email TEXT, gender TEXT, country TEXT, heartbeat TEXT, status TEXT,position TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTablePosts = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS posts(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picname TEXT, title TEXT, notes TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableReports = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS reports(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picture TEXT, reporter TEXT, message TEXT, type TEXT, position TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableForum = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS forum(ID INTEGER PRIMARY KEY ASC, serverdb_id TEXT, forumtopic TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.addForum = function (serverdb_id,forumtopic, type ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO forum(serverdb_id,forumtopic,type) VALUES (?,?,?)",
                    [serverdb_id,forumtopic,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }


        fasttrackdb.webdb.addReport = function (serverdb_id,picture,reporter,message,type,position) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO reports(serverdb_id,picture,reporter,message,type,position) VALUES (?,?,?,?,?,?)",
                    [serverdb_id,picture,reporter,message,type,position],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.addPost = function (serverdb_id,picname,title,notes,type) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO posts(serverdb_id,picname,title,notes,type) VALUES (?,?,?,?,?)",
                    [serverdb_id,picname,title,notes,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.onError=function(){
          alert("");
        }

fasttrackdb.webdb.addUser = function (fullname,email,phone,address,country,gender,heartbeat,status,position ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO users(phone,fullname,address,email,gender,country,heartbeat,status,position) VALUES (?,?,?,?,?,?,?,?,?)",
                    [phone,fullname,address,email,gender,country,heartbeat,status,position],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }
  dbresult=[];
  dblength=0;

function getUsers(query,callback){ // <-- extra param
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            fullname:row['fullname'],
            email:row['email'],
            country:row['country'],
            phone:row['phone'],
            gender:row['gender'],
            address:row['address'],
            heartbeat:row['heartbeat'],
            status:row['status'],
            position:row['position']
           
          }
        }
      //  console.log(dbresult);
        callback(dbresult);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 

dbitems=[];

function getPosts(query,callback){ // <-- extra param
 // dbresult="";

    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbitems[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            title:row['title'],
            notes:row['notes'],
            type:row['type'],
            picname:row['picname']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(dbitems); 
      });
   });
   
} 

function getReports(query,callback){ // <-- extra param
     
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            message:row['message'],
            reporter:row['reporter'],
            type:row['type'],
            picture:row['picture'],
            serverdb_id:row['serverdb_id'],
            position:row['position']
             
           
          }
        }
      //console.log(dbresult);
        callback(dbresult); 
      });
   });
   
} 

forum=[];

function getForum(query,callback){ // <-- extra param
    console.log(query);
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          forum[i]={
            id:row['ID'],
            forumtopic:row['forumtopic'],
            serverdb_id:row['serverdb_id'],
            type:row['type']
            
           
          }
        }
      //  console.log(dbresult);
        callback(forum);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 


var ROOT = "http://localhost/mobileapps/emergency/dashboard/admin";
//var ROOT="http://www.gsffuta.com/dashboard/admin";
 API = ROOT;
 console.log(API);
pinbox = "";
var model = new Iugo({
    firstname: "",
    fullname: "",
    email:"",
    aop:"",
    has_attend:"",
    not_attend:"",
    total_cases:"",
     patientname:"",
     case_content:"",
     emergency:  {
         title:"",
         content:"",
         picture:"",

     },
     case:  {
         content:"",
         picture:"",
         type:"",
         reporter:""


     }
      
});

function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}



function loadPhone()
{
  if ($('#register_phone').val().length  == 0)
  {
    $('#register_phone').val('+234');
  }
}

function loadPhone2()
{
  if ($('#login_phone').val().length  == 0)
  {
    $('#login_phone').val('+234');
  }
}

function formatToNaira(num) {
      
      
    var p = num.toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return   num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}

function switchMenu(page)
{
  console.log(page);
  
  app.menu.setMainPage(page);
   app.menu.closeMenu();


  app.menu.off("postclose", function() {

   
  
});


  app.menu.on("postclose", function() {
 
if (page=="transactions.html") {
  setTimeout(getTransactions,1000);

  //alert('');
};

});

 


 
}

questionIndex=0;

function nextSignup(){

questionIndex+=1;
usertype=$("#usertype").val();

agenttype=$("#mfc").val();
var fullname=  ( $('input#fullname').val());
var phone =  ( $('input#Phonenumber').val());
var email =  ( $('input#email').val());
var pin =  ( $('input#pin').val());

 
 console.log("usertype" +usertype);    


  
  if(questionIndex==0){
    $("#question").text("Are you an Emergency Agent");
  }

  if((usertype=="Yes")){

    if(questionIndex==1){
     $("#question").text("Which Emergency agency do you belong");
     $("#details").append('<select class="width-full" id="mfc"><option >Medical</option><option>MFC</option><option>Fire</option><option>Crime</option></select>')
     $("#usertype").hide();
  }

  if(questionIndex==2){
     $("#question").text("What is your name?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="fullname" value="" id="fullname" >');
      $("#mfc").hide();
  }

  if(questionIndex==3){
     $("#question").text("Your Phone Number and Email Address?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="Phonenumber" value="" id="Phonenumber" ><input type="text" class="text-input--underbar width-full"  placeholder="Email Address" value="" id="email" >');
      $("#fullname").hide();
  }

  if(questionIndex==4){
     $("#question").text("Your 5 digit fastTrack Token?");
     $("#details").append('<input type="tel" class="text-input--underbar width-full"  placeholder="Pin" value="" id="pin" >');
     $("#email").hide();
     $("#Phonenumber").hide();
  }

  if(questionIndex==5){
    var capture_pin =  ( $('input#pin').val()); 

  if (capture_pin == "") {
      showalert("Please Enter a Valid PIN");
      $('input#pin').focus();
      
      questionIndex=4
      return;
  }
  
  if (capture_pin.length != 5) {
      showalert("PIN must be exactly 5 digits");
      $('input#pin').focus(); 
      questionIndex=4;
      return
  } 
 
  
  var hashpass = CryptoJS.MD5(capture_pin);
  pin = hashpass.toString(); 
  }

  if (questionIndex==5){
    
    registerAgent(fullname,agenttype,email,phone,pin);
  }

  if(questionIndex==7){
      signupmodal.hide();
      main_navigator.pushPage('link.html');
    
    setTimeout(showactivity,200); 
    localStorage.setItem("usertype","agent");
  }


  }else{

     

  if(questionIndex==1){
     $("#question").text("What is your name?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="fullname" value="" id="fullname" >');
      $("#usertype").hide();
  }

  if(questionIndex==2){
     $("#question").text("Your Phone Number and Email Address?");
     $("#details").append('<input type="text" class="text-input--underbar width-full"  placeholder="Phonenumber" value="" id="Phonenumber" ><input type="text" class="text-input--underbar width-full"  placeholder="Email Address" value="" id="email" >');
      $("#fullname").hide();
  }

  if(questionIndex==3){
    registerUser(fullname,email,phone);
  }


  }
}

function showSignup(){
console.log("ddd");
  signupmodal.show();

  return;
   
  console.log(alternate);
  alternate=0;

  //main_navigator.pushPage("signup.html", { animation: "lift" }):
   main_navigator.pushPage('signup.html',{ animation: "lift" }); 
  setTimeout(hideForm,150);

}

function hideForm(){
   $("#userform").hide();
   $("#emergencyform").show();
}

function showForm(){
    $("#userform").show();
   $("#emergencyform").hide();
}

alternate=0;

function alternateForm(){
  if(alternate==0){
    showForm();
    alternate=1;
  }else{
    alternate=0;
    hideForm();
  }
  
}



function registeruser(){
  //var ROOT = "http://localhost/mobileapps/emergency/dashboard/admin/";
// var ROOT = "http://www.gsffuta.com/dashboard/admin";
  //     API = ROOT;
       
             
  
    var firstname =  ( $('input#user_firstname').val());
    var lastname =  ( $('input#user_lastname').val());
    var email =  ( $('input#user_email').val());
     var phone =  ( $('input#user_phone').val());
     var gender = $('input#user_gender').val();
     var address=$('input#user_address').val();
     var pin=$('input#user_pin').val();
    
    pin=CryptoJS.MD5(pin);
    pin=pin.toString();

    if (firstname == "") {
      alert("Please Enter Firstname.");
      $('input#user_firstname').focus();
      unspin(); 
      return;
    } else if (lastname == "") {
      showalert("Please Enter Lastname");
      $('input#user_lastname').focus();
      unspin();
      return
    }
    else if (email == "") {
      showalert("Please Enter Email Address.");
      $('input#user_email').focus();
      unspin();
      return
    }
  

   else if (phone == "" || phone.length < 10  ){
    showalert("Please Enter a Valid Phone Number.");
      $('input#user_phone').focus();
      return
    }
  

  
   
    
  if( !validateEmail(email)) { 
      
      showalert("Please Enter a Valid Email Address.");
      $('input#user_email').focus();
      unspin();
      return
      
  }

  
  

  if (gender == '')
  {
    showalert('Please select your gender');
    return;
  }

  var capture_pin =  ( $('input#user_pin').val()); 

  if (capture_pin == "") {
      showalert("Please Enter a Valid PIN");
      $('input#register_pin').focus();
      unspin(); 
      return;
  }
  
  if (capture_pin.length != 5) {
      showalert("PIN must be exactly 5 digits");
      $('input#register_pin').focus(); 
      unspin();
      return
  } 
  
  
  
  var hashpass = CryptoJS.MD5(capture_pin);
  hashpass = hashpass.toString(); 
 
  
  
 
  
               
 var request = $.ajax({
  url: API + '/registeruser/',
  type: "POST",
   data: {
         email: email,
         phone: phone,
         firstname: firstname,
         lastname: lastname,
         gender: gender,
         address:address,
         pin:pin
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
 console.log(msg);
 unspin();
localStorage.setItem("usertype","user");
localStorage.setItem("userprofile",msg);
//main_navigator.pushPage('user.html');
main_navigator.pushPage('login.html');
   //alert(msg);
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});
  
}



function registerAgent(fullname,agenttype,email,phone,pin)
{
     // spin();
      
      //var ROOT = "http://localhost/mobileapps/emergency/dashboard/admin";
    // var ROOT = "http://www.gsffuta.com/dashboard/admin";
      // API = ROOT;
       
            
  
   /* var firstname =  ( $('input#register_firstname').val());
    var lastname =  ( $('input#register_lastname').val());
    var email =  ( $('input#register_email').val());
     var phone =  ( $('input#register_phone').val());
     var gender = $('#gender').val();
     var agenttype=$('#agenttype').val();
    

    if (firstname == "") {
      showalert("Please Enter Firstname.");
      $('input#register_firstname').focus(); 
      unspin();
      return
    } else if (lastname == "") {
      showalert("Please Enter Lastname");
      $('input#register_lastname').focus();
      unspin();
      return
    }
    else if (email == "") {
      showalert("Please Enter Email Address.");
      $('input#register_email').focus();
      unspin();
      return
    }
  

   else if (phone == ""  ){
      showalert("Please Enter a Valid Phone Number.");
      $('input#register_phone').focus();
      unspin();
      return
    } 
    
  if( !validateEmail(email)) { 
     // showalert("Please Enter a Valid Email Address.");
     // $('input#register_email').focus();
      //return 
  }

  country=$("#country").val();
   
  if (gender == '')
  {
    showalert('Please select your gender');
    unspin();
    return;
  } 
  var capture_pin =  ( $('input#register_pin').val()); 

  if (capture_pin == "") {
      showalert("Please Enter a Valid PIN");
      $('input#register_pin').focus();
      unspin(); 
      return;
  }
  
  if (capture_pin.length != 5) {
      showalert("PIN must be exactly 5 digits");
      $('input#register_pin').focus(); 
      unspin();
      return
  } 
  
  var tosend = localStorage.getItem("capture_send_code");
  
  var hashpass = CryptoJS.MD5(capture_pin);
  hashpass = hashpass.toString(); 

  */

  var request = $.ajax({
  url: API + '/registeragent/',
  type: "POST",
   data: {
         email: email,
         phone: phone,
         fullname: fullname,
         agenttype: agenttype,
         pin: pin
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
  request.done(function( msg ) {
    //showalert("")
    $("#question").html("Now its time to make the world a safer place to live<br/>click next to get started");
    $("#details").hide();
    localStorage.setItem('profile',msg); 
    msg=jQuery.parseJSON(msg);
    console.log(msg.firstname);  
    if(msg.fullname != ""){
         
  }

 // pushReg(agenttype);
   
   /* main_navigator.pushPage('link.html');
    
    setTimeout(showactivity,200); 
    localStorage.setItem("usertype","agent");
    */
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});
  
}

function pushReg(agenttype){
  if(agenttype=="Crime"){
    initPushwooshCrime();
  }else if(agenttype=="Health"){
    initPushwooshHealth();

  }else{
    initPushwooshFire();
  }
}

function account(){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.fullname=profile.fullname
  model.email=profile.email;
  model.aop="Agent Type: "+profile.agenttype;
  model.has_attend=hstatus;
  model.not_attend=nstatus;
  model.total_cases=dblength;
}

nstatus=0;
hstatus=0;

function loginuser(){
  main_navigator.pushPage('user.html');
}

device_id='agent';

function alterlogin(){
  if(device_id=="agent"){
    device_id="user"
  }else{
    device_id="agent";
  }
}

idd=0;


function showactivity(){
  //dbresult='';
  $(".timeline").text("");
  if(typeof(localStorage.profile)!="undefined"){
  profile=jQuery.parseJSON(localStorage.profile);
  //pushReg(profile.agenttype);

  model.firstname=profile.fullname;
}
  getReports("select * from reports", function(callback){
    picture=''; iconn='';

    if(dbresult.length==0){
      $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    for(i=0; i<dbresult.length; i++){
      serverdb_id=dbresult[i].serverdb_id;

      picture="images/"+articletype(dbresult[i].type);

      if(dbresult[i].picture!=""){
        picture="data:image/jpg;base64,"+dbresult[i].picture;
      }

      //if(dbresult[i].status=="0"){
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }



      $(".timeline").append('<ons-list-item onclick="casedetails('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbresult[i].reporter+'</span><span class="timeline-id"></span></div><div class="timeline-message">Report Type: <i>'+dbresult[i].type+'</i></div></ons-col></ons-row></ons-list-item>');
    }
  });
}

serverdb_id=0;

function pullReports(){
  getPosts("select * from reports", function(callback){
    id=0;
    for(i=0;i<dbresult.length; i++){
      id=dbresult[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id);

     

    profile=jQuery.parseJSON(localStorage.profile);
    var agenttype= profile.agenttype;
    agenttype=agenttype.toLowerCase();

    split_agent=agenttype.split(" ");

    console.log(split_agent);
    agenttype=split_agent[0];

    console.log(agenttype);

   


  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getReports/',
  type: "POST",
  timeout:20000,
   data: {
          id:id,
          agenttype:agenttype
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addReport(msg[i].id,msg[i].picture,msg[i].reporter,msg[i].message,msg[i].type,msg[i].position);
    }
     
    }else{
      showalert("Report is updated");
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

function articleinit(){

}

function pullArticles(){
  getPosts("select * from posts", function(callback){
    id="";

    for(i=0;i<dbitems.length; i++){
      id=dbitems[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id)

  $("#loadmore").text("Loading...");
  var request = $.ajax({
  url: API + '/getPosts/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadmore").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addPost(msg[i].id,msg[i].picname,msg[i].post_title,msg[i].post_notes,msg[i].post_category);
    }
     
    }

    //showArticles();
     showalert("News updated");
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}

newstype="";

function showArticles(type){
  newstype=type
  //dbitems=[];
  console.log(dbitems);

  $(".timeline").html("");
   
     main_navigator.pushPage('articles.html',{ animation: "slide" }); 

     setTimeout(function(){
    getPosts("select * from posts where type = '"+type+"'", function(callback){
    
    console.log(dbitems);

    if(dbitems.length==0){
      $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">oops! help is coming soon</span></ons-list-item>');
      return;
    }

    for(i=0; i<dbitems.length; i++){ 

      id=dbitems[i].id;
      
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa-ambulance fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=articletype(dbitems[i].type); 

      $(".timeline").append('<ons-list-item onclick="article('+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="'+dbitems[i].picname+'"/></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+dbitems[i].title+'</span><span class="timeline-id"></span></div><div class="timeline-message">Type: <i>'+dbitems[i].type+'</i></div></ons-col></ons-row></ons-list-item>');
    
    }

  });
   },100);
  
   return; 
}

function listArticles(){
   
}

function articletype(type){
  type= type.toLowerCase();
   if(type=="fire"){
       return 'ambulance.png';
      }else if(type=="health"){
        return 'doctor.png'
      }else if(type=="crime"){
        return 'police.ico';
      }else{
       return 'ambulance.png';
      }
}

function article(id){
   idd=id;
  main_navigator.pushPage('emergency.html');
  picture =articletype(dbitems[idd].type);
  setTimeout(function(){
    $(".case_status").append('<img style="height:200px;" src="'+dbitems[idd].picname+'"/>');
     model.emergency.title=dbitems[idd].title;
     model.emergency.content=dbitems[idd].notes;
    usermap3();
   },100);
  
}

 

function casedetails(id){
  idd=id;
  main_navigator.pushPage('case.html');
   

   if(dbresult[idd].position==""){
    caseposition='<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">Case Location Not Available</span></ons-list-item>';
   $("#usermap").hide();
   }else{
   latlng=dbresult[idd].position.split("/");
    
   location.lat=latlng[0];
   location.lng=latlng[1];
    caseposition='';
   }

   if(dbresult[id].picture==""){
    picture ="images/"+articletype(dbresult[idd].type);
  }else{
    picture="data:image/jpg;base64,"+dbresult[id].picture
  }

   
   

   setTimeout(function(){
    $(".case_status").append('<img style="height:200px;" src="'+picture+'"/>');
    model.case.reporter=dbresult[idd].reporter;
    model.case.content=dbresult[idd].message;
    usermap3();
   },100);
 
}

 

function listChatPals(){
  $(".timeline").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Messages Yet</span></ons-list-item>');
}




function usermap(){
  if(typeof(google)=="undefined"){
    return;
  }
           var mapCanvas = document.getElementById('userlocation');
           console.log("location is "+location.lat," , "+location.lng);
            var mapOptions = {
          //6.5243793/3.3792057

          center: new google.maps.LatLng(location.lat,location.lng),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }


        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var myLatlng = new google.maps.LatLng(location.lat,location.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Mumbai"
        });
        }   


        function usermap3(){
          if(typeof(google)=="undefined"){
            return;
          }
          console.log(location.lat);
           var mapCanvas = document.getElementById('usermap');
           console.log("location is "+location.lat," , "+location.lng);
            var mapOptions = {
          //6.5243793/3.3792057

          center: new google.maps.LatLng(location.lat,location.lng),
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var myLatlng = new google.maps.LatLng(location.lat,location.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Mumbai"
        });
        }   

        function usermap2(){

           if(typeof(google)=="undefined"){
            return;
            }
           
           var mapCanvas = document.getElementById('mapBox');
           console.log("location is "+location.lat," , "+location.lng);
            var mapOptions = {
          //6.5243793/3.3792057

        center: new google.maps.LatLng(location.lat,location.lng),
        //center: new google.maps.LatrootLng("18.9750", "72.8258"),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(mapCanvas, mapOptions)

        var myLatlng = new google.maps.LatLng(location.lat,location.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: "Mumbai"
        });
        }   


function logininit(){
 //main_navigator.pushPage('user.html');
 //return;

   

 if(typeof(google)!="undefined"){

  google.maps.event.addDomListener(window, 'load', init);

        var map;
      }

 if(typeof(localStorage.profile) !="undefined"){
    main_navigator.pushPage('pin.html',{ animation: "lift" });
    return;
 }

 if(typeof(localStorage.userprofile)!="undefined"){
    setTimeout(usermap2,100);
      main_navigator.pushPage('user.html',{ animation: "lift" });
    return;
 }

  // main_navigator.pushPage('login.html',{ animation: "lift" });
  loginmodal.show();
 return;


 
   
}

function userlogin(){
  spin();

  console.log('userlogin'); 
var pin =  ( $('input#login_pin').val()); 
    
    if (pin == "") {
      showalert("Please Enter your PIN Number.");
      $('input#login_pin').focus(); 
      unspin();
      return
    }
  
  var phone = $('input#login_phone').val();

  
   if (pin == "") {
      showalert("Please Enter your Phone Number");
      $('input#login_phone').focus(); 
        unspin();
      return
    }
//console.log(phone);

  var hashpass = CryptoJS.MD5(pin);
  hashpass = hashpass.toString();

   if((typeof(localStorage.userprofile) !="undefined") && (localStorage.userprofile !="")){
   profile=jQuery.parseJSON(localStorage.userprofile);
   if(phone==profile.phone){
    main_navigator.pushPage('user.html');
    return; 
   }
   
  }else{

     var request = $.ajax({
  url: API + '/verifyuser/',
  type: "POST",
   data: {
          
         phone: phone, 
         password: hashpass, 
         }
  
});

  
request.done(function( msg ) {
  unspin();
  console.log(msg);
  //return; 

  nmsg=msg;
  msg=jQuery.parseJSON(msg);  
  
if(msg.length > 0){ 
    msg=JSON.stringify(msg[0]);
    console.log(msg)
   // return;

     

  main_navigator.pushPage('user.html');
  localStorage.setItem("userprofile",msg);
  localStorage.setItem("usertype","user");
  setTimeout(usermap2,100); 
}else{
  showalert("Invalid Pin or Phonenumber");

}
   
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
   setTimeout(showactivity,300); 
  //alert('check internet connection');
});
            

  }

}

profile=[];

function login()
{

  var usertype=$("#usertype").val();
  console.log(usertype);

  if(usertype=="0"){
    showalert("Please Select MFC or User login");
    return;
  }
  
  if(usertype=="user"){
    userlogin();
    return;
  }
   

spin(); 
  
var pin =  ( $('input#login_pin').val()); 
    if (pin == "") {
      showalert("Please Enter your PIN Number.");
      $('input#login_pin').focus(); 
      unspin();
      return
    }
  
  var phone = $('input#login_phone').val();
   if (phone == "") {
      showalert("Please Enter your Phone Number");
      $('input#login_phone').focus(); 
        unspin();
      return
    }


  var hashpass = CryptoJS.MD5(pin);
  hashpass = hashpass.toString();

   if(typeof(localStorage.profile) !="undefined"){
   // alert('');
   profile=jQuery.parseJSON(localStorage.profile);
   if(phone==profile.phone){
    alert('');
    main_navigator.pushPage('link.html');
    setTimeout(showactivity,300);
    return; 
   }
   
  } 

   var request = $.ajax({
  url: API + '/verifyagent/',
  type: "POST",
   data: {
          
         phone: phone, 
         password: hashpass, 
         }
  
});

  
request.done(function( msg ) {
  unspin();

  nmsg=msg;
  msg=jQuery.parseJSON(msg);  
  console.log(msg);

if(msg.length > 0){ 
    msg=JSON.stringify(msg[0]);
    console.log(msg)
   // return;

    
loginmodal.hide();
  main_navigator.pushPage('link.html');
  localStorage.setItem("profile",msg);
  localStorage.setItem("usertype","agent");
  setTimeout(showactivity,300); 
}else{
  showalert("Invalid Pin or Phonenumber");

}
   
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
   setTimeout(showactivity,300); 
  alert('check internet connection');
});
            
  
}

function reload(){
  window.location.reload();
  return;
   main_navigator.pushPage('start.html',{ animation: "none" });
}

location.lat='222';
location.lng='777';


function onPositionUpdate(position)
            {

                 location.lat = position.coords.latitude;
                 location.lng = position.coords.longitude;

                 console.log("Current position: " + location.lat + " " + location.lng);
            }
 
 
























function initPushwoosh()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "64412-91F0F" });
  alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
            alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
     alert('push started 3');
}


 //ons.bootstrap();




function init()
{

 
  //localStorage.setItem("usertype","user");
  document.addEventListener("deviceready", initPushwoosh, true);
  //document.addEventListener("deviceready", initPushwooshCrime, true);
  //document.addEventListener("deviceready", initPushwooshHealth, true);


if(navigator.geolocation){
              
               navigator.geolocation.getCurrentPosition(onPositionUpdate);
            }else
               console.log("navigator.geolocation is not available");



  fasttrackdb.webdb.open();
  fasttrackdb.webdb.createTable();
  fasttrackdb.webdb.createTablePosts();
  fasttrackdb.webdb.createTableReports();
  fasttrackdb.webdb.createTableForum();

 main_navigator.pushPage('index.html',{ animation: "none" });

  
  //setTimeout( ons.bootstrap,100);


 

 // document.addEventListener("deviceready", initPushwoosh, true);

//getDeviceHandler();
    
}

function showothers(){
  main_navigator.pushPage('others.html');
  //$("#push_message").text("My current location is "+location.lat+ " , "+location.lng);
  setTimeout(initothers,300);
}

function initothers () {
 $("#push_message").text("My current location is "+location.lat+ ","+location.lng);
}

function initPushMessage(){
 

}

function pushMessage(pushType) {
   main_navigator.pushPage('gps.html');
   setTimeout(initPushMessage,100);
   setTimeout(usermap,100);
    
   linkname="";
   if(pushType=="crime"){
    linkname='/pushCrime';
    message="There is an ongoing criminal activity going on at latitude "+location.lat+" and Longitude "+location.lng+". Please respond urgently and safe lives and propertis";
   }else if(pushType=='fire'){
    linkname='/pushFire';
    message="There is a fire outbreak at latitude "+location.lat+" and Longitude "+location.lng+". Please respond urgently and safe lives and properties";
   }else{
    linkname='/pushHealth';
    message="A medical attention is needed at latitude "+location.lat+" and Longitude "+location.lng+". Please respond urgently and safe lives and properties";
   }

   var user=localStorage.userprofile;
   user=jQuery.parseJSON(user);
   reporter=user.firstname+' '+user.lastname;
    
   type=pushType;


   var request = $.ajax({
  url: API + linkname,
  type: "POST",
   data: {
         message: message+location.lat+"/"+location.lng,
         reporter:reporter,
        
         type:type,
         position:location.lat+"/"+location.lng
          
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
 console.log(msg);
 $("#pushStatus").text("Notification Sent");
 unspin();
 
      
  
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});

  
}

function showMessage(){
  main_navigator.pushPage('agents.html');
}

function snapSend(){
  snapshot();
  notifyUsers();
}


function notifyUsers(){

   
   $("#load_msg").text("Sending Notification to Citizens");
spin();
  var push_message=$('#push_message').val();
  var post_title=$('#push_title').val();
  //push_message=replaceAll(push_message); 
   
  console.log(push_message);

  var user=localStorage.profile;
   user=jQuery.parseJSON(user);
   reporter=user.firstname+' '+user.lastname;
    
    if(newstype==""){
      type="Custom";
    }else{
      type=newstype;
    }
   
   picture=imgreport;
   picture="data:image/jpg;base64,"+picture;
   //alert(picture);

   
  var request = $.ajax({
  url: API + '/notifyUsers',
  type: "POST",
  timeout:20000,
   data: {
        post_notes:push_message,
        post_title:post_title,
        post_category:type,
        picname:picture

        
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
console.log(msg);
$("#load_msg").text("Citizens Notified");
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}


function updateLocation(){
  console.log(profile);
  console.log(location.lat);

  var request = $.ajax({
  url: API + '/updateLocation',
  type: "POST",
  timeout:20000,
   data: {
          id:profile.id,
         location:location.lat+'/'+location.lng
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
console.log(msg);
showalert("Location updated");
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}


function pushSms(){
   
   $("#load_msg").text("Notifying MFC agents via sms ...");
spin();
  var push_message=$('#push_message').val();
  push_message=replaceAll(push_message); 
   
  console.log(push_message);

  var user=localStorage.userprofile;
   user=jQuery.parseJSON(user);
   reporter=user.firstname+' '+user.lastname;
   picture=imgreport;
   alert(picture);
   type='Custom';
   
  var request = $.ajax({
  url: API + '/sendsms',
  type: "POST",
  timeout:20000,
   data: {
         message:push_message,
         reporter:reporter,
         picture:picture,
         type:type,
         position:location.lat+'/'+location.lng
        // secret : 'f9b45f5b3e6f1737804dc5d1f9c39202'
         }
  
});

  
request.done(function( msg ) {
unspin();
console.log(msg);
$("#load_msg").text("MFC agents notified");
});

request.error(function( msg ) {
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}

 
function initPushwooshHealth()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "75E3D-FAB34" });
 // alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
      //      alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
    // alert('push started 3');
}

function initPushwooshFire()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "CB083-05399" });
  //alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
       //     alert('push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
    // alert('push started 3');
}

function initPushwooshCrime()
{
   // alert('push started');
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
 
    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_NUMBER", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "703630268544", appid : "A1E9F-32A6D" });
  //alert('push started2');
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
            alert('crime push token: ' + pushToken);
        },
        function(status) {
            //console.warn(JSON.stringify(['failed to register ', status]));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );
     //alert('push started 3');
}


 var onCardIOComplete = function(response) {
        
        $('#card_number').val(response.card_number)      
        $('#card_mm').val(response.expiry_month)      
        $('#card_yy').val(response.expiry_year)      
        $('#card_cvv').val(response.cvv)   

        $('#card_number').trigger( "change" );   
  
      };
  
      var onCardIOCancel = function(response) {
      //  showalert ("test")
      //  alert(JSON.stringify(response));
          showalert("card.io scan cancelled");
      };


function cardReader() {
              CardIO.scan({
                  "collect_expiry": true,
                  "collect_cvv": false,
                  "collect_zip": false,
                  "shows_first_use_alert": true,
                  "disable_manual_entry_buttons": false
                },
                onCardIOComplete,
                onCardIOCancel
              );
            }
      



 

 

function fillPin()
{
  $("#pinbox").html("");


  for (var i=0;i<pinbox.length;i++)
  {
  
    $("#pinbox").append("<i class='fa fa-circle'> </i>");
    
  }
   //return;
//console.log(pinbox.length);
  var balance = 5 - pinbox.length;
//console.log(balance);
  for (var i=0;i<balance;i++)
  {
  
    $("#pinbox").append("<i class='fa fa-circle-thin'> </i>");
    
  }

}
function PINEntry(key) {
  
  
  
  if (key == "-") 
    {
      pinbox = pinbox.substr(0, pinbox.length-1);
       $("#pinbox").html("");
      fillPin();
      return;
    }

  else if (pinbox.length < 6) pinbox = pinbox+key;
  
  
  $("#pinbox").html("");
  
  fillPin();
  if (pinbox.length == 5) {
    //console.log(pinbox);
    var hashpass = CryptoJS.MD5(pinbox);
    
    
   // var capturepin = localStorage.getItem("capture_pin");
   profile=jQuery.parseJSON(localStorage.profile);

   var capturepin= (profile.pin);
    //console.log(profile.password);
   //console.log(hashpass.toString());
    
    if (hashpass.toString() == capturepin) {
    
       //main_navigator.pushPage('main.html');
     //pushReg(profile.agenttype);
       main_navigator.pushPage('link.html');
      setTimeout(showactivity,200);
      
    }
    else {
      authenticated = false;
      
      showalert ('Wrong PIN.');
      
      
    }
    
    pinbox = "";
    $("#pinbox").html("");
    fillPin();
  }
}










function showMenu(){
  main_navigator.pushPage('menu.html');
}


function showforum(){
  main_navigator.pushPage('forum.html');

   

  if(typeof(localStorage.profile)!="undefined"){
  profile=jQuery.parseJSON(localStorage.profile);
  console.log(profile);

  model.firstname=profile.fullname;
}

setTimeout(function(){
      getForum("select * from forum", function(callback){
    picture=''; iconn='';

    $('.forum').html("");
    console.log(forum);

    if(forum.length==0){
     // $("#forumstatus").hide();
     $(".forum").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }
    for(i=0; i<forum.length; i++){
 
     if(i % 2 ==0){
      nstatus+=1;
        iconn='<i class="fa fa- fa-2x status0"></i>';
      }else{
        hstatus+=1;
        iconn='<i class="fa fa-check fa-2x status1"></i>';
      }

      picture=articletype(forum[i].type);
      forumtopic="";

       for(j=0; j<forum[i].forumtopic.length; j++){
        if(j<=30){
        forumtopic+=forum[i].forumtopic[j];
      }
      }
      forumtopic+="...";


      $(".forum").append('<ons-list-item onclick="forumdetails('+forum[i].id+','+i+')" class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="50px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 50px; max-width: 50px;"><img  class="timeline-image" src="images/'+picture+'"></ons-col><ons-col class="col ons-col-inner"><div class="timeline-date">'+iconn+'</div><div class="timline-from"><span class="timeline-name"><b>'+forumtopic+'</span><span class="timeline-id">'+'  </span></div><div class="timeline-message"></i></div></ons-col></ons-row></ons-list-item>');
    }
  });
   },100);


}


function pullForum(){
  getForum("select * from forum", function(callback){
   
    id=0;

    for(i=0;i<forum.length; i++){
      id=forum[i].serverdb_id;
    }
    if(id==""){
      id=0;
    }

    console.log(id);

  $("#loadforum").text("Loading...");
  // console.log(forum);
  var request = $.ajax({
  url: API + '/getForum/',
  type: "POST",
  timeout:20000,
   data: {
          id:id
         }
  
});

  
  request.done(function( msg ) {
    $("#loadforum").text("Load more");
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
    fasttrackdb.webdb.addForum(msg[i].id,msg[i].forumtopic,msg[i].type);
    }
     
    }else{
      showalert("Forum updated");
    }

    showforum();
     
  
});

request.error(function( msg ) {
  $("#loadforum").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
});

}



function forumdetails(id,index){
  idd=id;

  console.log("forum id "+idd);
   main_navigator.pushPage('message.html');
   //setTimeout(pullmessage,100);
   setTimeout(function(){
    console.log(forum);
    $("#forumtopic").text(forum[index].forumtopic);
     $("messages").html("");
  var request = $.ajax({
  url: API + '/pullMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd
         }
  
});

  
  request.done(function( msg ) {
 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length==0){
     // $("#forumstatus").hide();
     $(".messages").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
      $("#messages").append("<div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div>");
      //$("#msgbody").append("<tr><td><div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div></td></tr>");
    }

    $('#bdy')[0].scrollTop = $('#bdy')[0].scrollHeight;
     
    } else{
      $(".messages").append('<ons-list-item class="timeline-li ng-scope list__item ons-list-item-inner list__item--tappable" modifier="tappable"><span class="colortext">No Reported Case Yet</span></ons-list-item>');
      return;
    }

    showactivity();
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});
   },100);
}

function pullmessage(){
  

}

function sendMessage(){

  if(typeof(localStorage.profile)!="undefined"){
     profile=jQuery.parseJSON(localStorage.profile);
  }else if(typeof(localStorage.userprofile)!="undefined"){
     profile=jQuery.parseJSON(localStorage.userprofile);
  }else{
    return;
  }

  console.log(profile);

  sender=profile.fullname;
  var msg=( $('input#msg').val());
  //console.log(msg);

  var request = $.ajax({
  url: API + '/forumMessage/',
  type: "POST",
  timeout:20000,
   data: {
          forumid:idd,
          msg:msg,
          sender:sender
         }
  
});

  
  request.done(function( msg ) {

    $("#messages").html("");
    $('input#msg').val("");
    $('input#msg').focus();

 
    unspin();
     
    msg=jQuery.parseJSON(msg);
    console.log(msg);

    if(msg.length > 0){
    
    for(i=0; i<msg.length; i++){
      
     $("#messages").append("<div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div>");
     //$("#msgbody").append("<tr><td><div class='contentpod msg'><span class='highlight'><i>"+msg[i].sender+':</i></span>'+msg[i].msg+"</div></td></tr>");

     
    }

     
$('#bdy')[0].scrollTop = $('#bdy')[0].scrollHeight;
console.log($('#msgbody')[0].scrollHeight);

     
    }else{
      showalert("No Messages");
    }

    //$('#messages').scrollTo(500);
     
  
});

request.error(function( msg ) {
  $("#loadmore").text("Load more");
  unspin();
  console.log(msg);
  alert('check internet connection');
});

}































function showalert(message)
{
    try
    {
      // toast.showLong(message);
      window.plugins.toast.showShortCenter(message)
    }
    catch(err)
    {
      alert (message);
    }
}


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) ) {
    return false;
  } else {
    return true;
  }
}

 



function spin()
{
  $('.nospin').hide();
   $('.spinner').show();
   $('.button--large').attr('disabled','disabled');
}

function unspin()
{
  $('.nospin').show();
   $('.spinner').hide();
 
   $('.button--large').removeAttr('disabled');
} 
 

function deleteMe()
{

  navigator.notification.confirm(
    'Are you sure you want to delete your account?', // message
     onConfirm,            // callback to invoke with index of button pressed
    'Delete? ',           // title
    ['Yes! Delete','No, Cancel']     // buttonLabels
);


}
 


function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

 
function copyLink(){
  var text = "copied link";

cordova.plugins.clipboard.copy(text);

cordova.plugins.clipboard.paste(function (text) { alert(text); });
}

function sendmail(){
  window.location='mailto:joshua@kleindevort.com';
}

function snapshot(){
  navigator.camera.getPicture(onCameraSuccess, onFail, { quality: 50,
    destinationType: Camera.DestinationType.DATA_URL
});
}

imgreport='';

function onCameraSuccess(imgdata){
   var image = document.getElementById('myImage');
    image.src = "data:image/jpeg;base64," + imgdata;
    imgreport=imgdata;
    //alert(imgreport);
}

function onCameraFail(message){
  alert('Failed because: ' + message);
}

function sendSms(){
   alert('click');
    var number = document.getElementById('phone_no').value;
        var message = document.getElementById('sms_message').value;
        alert(number);
        alert(message);
        var intent = 'INTENT'; //leave empty for sending sms using default intent
        var success = function () { alert('Message sent successfully'); };
        var error = function (e) { alert('Message Failed:' + e); };
        sms.send(number, message, intent, success, error);

}

function replaceAll(str){
  var new_str='';
  for (var i = str.length - 1; i >= 0; i--) {
    if(str[i]==' '){
      str[i]='-';
      new_str += str[i];
    }

    str=str.replace(' ','-');

  };
  return str;
}







