
(function(){
  'use strict';
  var module = angular.module('app', ['onsen']);
 

       


 
})();


  
var fasttrackdb = {};
        fasttrackdb.webdb = {};
        fasttrackdb.webdb.db = null;

        fasttrackdb.webdb.open = function () {
            var dbSize = 5 * 1024 * 1024; // 5MB
            fasttrackdb.webdb.db = openDatabase("FastTrack", "1.0", "FastTrack", dbSize);
        }

        fasttrackdb.webdb.createTable = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY ASC, phone TEXT, fullname TEXT,address TEXT, email TEXT, gender TEXT, country TEXT, heartbeat TEXT, status TEXT,position TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTablePosts = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS posts(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picname TEXT, title TEXT, notes TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableReports = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS reports(ID INTEGER PRIMARY KEY ASC,serverdb_id TEXT, picture TEXT, reporter TEXT, message TEXT, type TEXT, position TEXT, time TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.createTableForum = function () {
           
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS forum(ID INTEGER PRIMARY KEY ASC, serverdb_id TEXT, forumtopic TEXT, type TEXT)", []);
                 
            });
           
        }

        fasttrackdb.webdb.addForum = function (serverdb_id,forumtopic, type ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO forum(serverdb_id,forumtopic,type) VALUES (?,?,?)",
                    [serverdb_id,forumtopic,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }


        fasttrackdb.webdb.addReport = function (serverdb_id,picture,reporter,message,type,position,time) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO reports(serverdb_id,picture,reporter,message,type,position,time) VALUES (?,?,?,?,?,?,?)",
                    [serverdb_id,picture,reporter,message,type,position,time],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.addPost = function (serverdb_id,picname,title,notes,type) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO posts(serverdb_id,picname,title,notes,type) VALUES (?,?,?,?,?)",
                    [serverdb_id,picname,title,notes,type],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }

        fasttrackdb.webdb.onError=function(){
          alert("");
        }

fasttrackdb.webdb.addUser = function (fullname,email,phone,address,country,gender,heartbeat,status,position ) {
            //alert("");
            var db = fasttrackdb.webdb.db;
            db.transaction(function (tx) {
                
                tx.executeSql("INSERT INTO users(phone,fullname,address,email,gender,country,heartbeat,status,position) VALUES (?,?,?,?,?,?,?,?,?)",
                    [phone,fullname,address,email,gender,country,heartbeat,status,position],
                    fasttrackdb.webdb.onSuccess,
                    fasttrackdb.webdb.onError
                    );
            });
        }
  dbresult=[];
  dblength=0;

function getUsers(query,callback){ // <-- extra param
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            fullname:row['fullname'],
            email:row['email'],
            country:row['country'],
            phone:row['phone'],
            gender:row['gender'],
            address:row['address'],
            heartbeat:row['heartbeat'],
            status:row['status'],
            position:row['position']
           
          }
        }
      //  console.log(dbresult);
        callback(dbresult);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 

dbitems=[];

function getPosts(query,callback){ // <-- extra param
 // dbresult="";

    console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbitems[i]={
            id:row['ID'],
            serverdb_id:row['serverdb_id'],
            title:row['title'],
            notes:row['notes'],
            type:row['type'],
            picname:row['picname']
             
           
          }
        }
      //console.log(dbresult);
     // dbresult=dbitems;
        callback(dbitems); 
      });
   });
   
} 

function getReports(query,callback){ // <-- extra param
     console.log(query);
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          dbresult[i]={
            message:row['message'],
            reporter:row['reporter'],
            type:row['type'],
            picture:row['picture'],
            serverdb_id:row['serverdb_id'],
            position:row['position'],
            time:row['time']
             
           
          }
        }
      //console.log(dbresult);
        callback(dbresult); 
      });
   });
   
} 



forum=[];

function getForum(query,callback){ // <-- extra param
    console.log(query);
    
    var db = fasttrackdb.webdb.db;
   db.transaction(function (tx) {
      tx.executeSql(query, [], function(tx, rs){
        dblength=rs.rows.length;
        for(i=0; i<rs.rows.length; i++){
          var row = rs.rows.item(i);
          forum[i]={
            id:row['ID'],
            forumtopic:row['forumtopic'],
            serverdb_id:row['serverdb_id'],
            type:row['type']
            
           
          }
        }
      //  console.log(dbresult);
        callback(forum);
        
         //console.log(transactdb);
        // callBack(dblength); // <-- new bit here
      });
   });
   
} 

//var baseurl="http://localhost/mobileapps/cordovapush/hello/dashboard";
var baseurl="http://www.gsffuta.com/dashboard";
var ROOT = "http://localhost/mobileapps/cordovapush/hello/dashboard/admin";
//var ROOT="http://www.gsffuta.com/dashboard/admin";
 API = ROOT;
 console.log(API);
pinbox = "";
var model = new Iugo({
    firstname: "",
    fullname: "",
    email:"",
    aop:"",
    has_attend:"",
    not_attend:"",
    total_cases:"",
     patientname:"",
     case_content:"",
     emergency:  {
         title:"",
         content:"",
         picture:"",

     },
     case:  {
         content:"",
         picture:"",
         type:"",
         reporter:""


     }
      
});


function createTable(){
  /* fasttrackdb.webdb.open();
  fasttrackdb.webdb.createTable();
  fasttrackdb.webdb.createTablePosts();
  fasttrackdb.webdb.createTableReports();
  fasttrackdb.webdb.createTableForum();

  */
}
 
 

function init()
{
   



main_navigator.pushPage('index.html',{ animation: "none" });

 setTimeout(function(){

     var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
         
         spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false

         
    });
   },100);

 
    
} 



function showSignup(){
  signupmodal.show();
}

account_type="";

function signuptype(type){
  account_type=type;
}

function signupForm(){
  if(account_type=="user"){
    main_navigator.pushPage('user_signup.html',{ animation: "lift" });
  }else if(account_type=="merchant"){
    main_navigator.pushPage('merchant_signup.html',{ animation: "none" });
  }else{
    showAlert("Please Select Sign Up Type");
  }

  signupmodal.hide();
}

function logininit(){
  main_navigator.pushPage('login.html',{ animation: "lift" });
}


function login(){
  if(account_type=="user"){
    main_navigator.pushPage('user_dashboard.html',{ animation: "lift" });
  }else if(account_type=="merchant"){

    main_navigator.pushPage('merchant_dashboard.html',{ animation: "none" });

  }else{
    showAlert("Please Select Login Type");
  }
}


function showAlert(message){
   swal(message);
}

function viewMerchants(){
  main_navigator.pushPage('list_of_merchants.html',{animation:"none"});
}

function viewProfile(){
  main_navigator.pushPage('profile_page.html',{animation:"none"});

}

function addMerchant(m){
  showAlert(m+' has been added to your list');
}

cot  = 0;
function count(){
 // showAlert('he');
 console.log(cot);
 cot +=1;
}

function viewCustomers(){
  main_navigator.pushPage('list_of_customers.html',{animation:"none"});
}

function viewcustomerProfile(){
   main_navigator.pushPage('customerprofile_page.html',{animation:"none"});
}
function addItem(){
  additemmodal.show();
}

sn = 0;
totalprice = 0;
function addeachitem(){
 price = document.getElementById('price').value;
  var itemname = document.getElementById('itemname').value;
  totalprice +=  Number(price);
  sn+=1;
  document.getElementById('totalprice').value="";
  $("#addItems").append('<ons-list-item  class="timeline-li ng-scope  ons-list-item-inner list__item--tappable tableinvoice" modifier="tappable" ><ons-row class="row ons-row-inner"><ons-col width="35px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 35px; text-align: center ;"><h4 class="invoicetext">'+sn+'</h4> </ons-col><ons-col width="2px" class="plan-center divider" ></ons-col><ons-col width="200px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 200px; text-align: center ; "><h4 class="invoicetext">'+itemname+'</h4></ons-col><ons-col width="2px" class="plan-center divider" > </ons-col> <ons-col width="100px" class="col ons-col-inner" style="-webkit-box-flex: 0; flex: 0 0 100px; text-align: center ; "><h4 class="invoicetext">'+price+'</h4></ons-col></ons-row></ons-list-item>');
 $("#totalprice").text(totalprice);
  document.getElementById('price').value = '';
  document.getElementById('itemname').value='';
  additemmodal.hide()
}
user_wallet
function user_walletlogin(){
     main_navigator.pushPage('user_wallet.html',{animation:"none"});
}


var chart;
            var legend;

            var chartData = [{
                country: "pending",
                litres:  301.90
            }, {
                country: "paid",
                litres: 128.30
            }, {
                country: "returns",
                litres: 99.00
            }, {
                country: "overflow",
                litres: 60.00
            }];


 AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "country";
                chart.valueField = "litres";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;

                // WRITE
                
            });

 function piechart(){
                 chart.write("piechart");
            }